import React from 'react';
import { AppBar } from '@material-ui/core';

export default function AppHeader(props) {
  return <AppBar {...props} color="inherit" />;
}
